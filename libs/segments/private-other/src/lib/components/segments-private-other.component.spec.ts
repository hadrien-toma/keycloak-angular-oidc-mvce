import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateOtherComponent } from './segments-private-other.component';


describe('SegmentsPrivateOtherComponent', () => {
  let component: SegmentsPrivateOtherComponent;
  let fixture: ComponentFixture<SegmentsPrivateOtherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateOtherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateOtherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
