import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'workspace-segments-private-other',
  templateUrl: './segments-private-other.component.html',
  styleUrls: ['./segments-private-other.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SegmentsPrivateOtherComponent  {
}
