import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateOtherComponent } from './components/segments-private-other.component';

@NgModule({
	declarations: [SegmentsPrivateOtherComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateOtherComponent,
        children: [
			{
			  path: '',
			  pathMatch: 'full',
			  redirectTo: 'very',
			},
          {
            path: 'very',
            loadChildren: () =>
              import('@workspace/segments-private-other-very').then(
                (module) => module.SegmentsPrivateOtherVeryModule
              ),
          },
          {
            path: 'very-aux',
            loadChildren: () =>
              import('@workspace/segments-private-other-very-aux').then(
                (module) => module.SegmentsPrivateOtherVeryAuxModule
              ),
          },
		  {
			path: '**',
			redirectTo: 'very',
		  },
        ],
      }
    ]),
  ],
})
export class SegmentsPrivateOtherModule {}
