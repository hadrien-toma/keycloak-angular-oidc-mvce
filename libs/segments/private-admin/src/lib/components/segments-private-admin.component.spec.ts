import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateAdminComponent } from './segments-private-admin.component';


describe('SegmentsPrivateAdminComponent', () => {
  let component: SegmentsPrivateAdminComponent;
  let fixture: ComponentFixture<SegmentsPrivateAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
