import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'workspace-segments-private-admin',
  templateUrl: './segments-private-admin.component.html',
  styleUrls: ['./segments-private-admin.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SegmentsPrivateAdminComponent  {
}
