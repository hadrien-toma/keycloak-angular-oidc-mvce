import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateAdminComponent } from './components/segments-private-admin.component';

@NgModule({
	declarations: [SegmentsPrivateAdminComponent],
  imports: [
    CommonModule,
	RouterModule.forChild([
		{
			path: '',
			component: SegmentsPrivateAdminComponent,
			children: []
		}
	]),
  ],
})
export class SegmentsPrivateAdminModule {}
