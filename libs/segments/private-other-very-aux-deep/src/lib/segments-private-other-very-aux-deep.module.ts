import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateOtherVeryAuxDeepComponent } from './components/segments-private-other-very-aux-deep.component';

@NgModule({
  declarations: [SegmentsPrivateOtherVeryAuxDeepComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateOtherVeryAuxDeepComponent,
        children: [
			{
			  path: '',
			  redirectTo: 'route',
			},
          {
            path: 'route',
            loadChildren: () =>
              import('@workspace/segments-private-other-very-aux-deep-route').then(
                (module) => module.SegmentsPrivateOtherVeryAuxDeepRouteModule
              ),
          },
          {
            path: 'route-aux',
            loadChildren: () =>
              import(
                '@workspace/segments-private-other-very-aux-deep-route-aux'
              ).then(
                (module) => module.SegmentsPrivateOtherVeryAuxDeepRouteAuxModule
              ),
          },
		  {
			path: '**',
			redirectTo: 'route',
		  },
        ],
      },
    ]),
  ],
})
export class SegmentsPrivateOtherVeryAuxDeepModule {}
