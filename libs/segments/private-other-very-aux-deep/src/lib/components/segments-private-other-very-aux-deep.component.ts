import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'workspace-segments-private-other-very-aux-deep',
  templateUrl: './segments-private-other-very-aux-deep.component.html',
  styleUrls: ['./segments-private-other-very-aux-deep.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SegmentsPrivateOtherVeryAuxDeepComponent  {
}
