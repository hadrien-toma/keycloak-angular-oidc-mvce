import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateOtherVeryAuxDeepComponent } from './segments-private-other-very-aux-deep.component';


describe('SegmentsPrivateOtherVeryAuxDeepComponent', () => {
  let component: SegmentsPrivateOtherVeryAuxDeepComponent;
  let fixture: ComponentFixture<SegmentsPrivateOtherVeryAuxDeepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateOtherVeryAuxDeepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateOtherVeryAuxDeepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
