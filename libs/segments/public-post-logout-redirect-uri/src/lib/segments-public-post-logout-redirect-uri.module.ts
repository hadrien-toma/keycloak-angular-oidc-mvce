import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPublicPostLogoutRedirectUriComponent } from './components/segments-public-post-logout-redirect-uri.component';

@NgModule({
	declarations: [SegmentsPublicPostLogoutRedirectUriComponent],
  imports: [
    CommonModule,
	RouterModule.forChild([
		{
			path: '',
			component: SegmentsPublicPostLogoutRedirectUriComponent,
			children: []
		}
	]),
  ],
})
export class SegmentsPublicPostLogoutRedirectUriModule {}
