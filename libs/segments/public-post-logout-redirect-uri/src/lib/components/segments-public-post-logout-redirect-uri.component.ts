import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'workspace-segments-public-post-logout-redirect-uri',
  templateUrl: './segments-public-post-logout-redirect-uri.component.html',
  styleUrls: ['./segments-public-post-logout-redirect-uri.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SegmentsPublicPostLogoutRedirectUriComponent  {
}
