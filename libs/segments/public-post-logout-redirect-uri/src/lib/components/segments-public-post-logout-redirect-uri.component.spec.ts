import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPublicPostLogoutRedirectUriComponent } from './segments-public-post-logout-redirect-uri.component';


describe('SegmentsPublicPostLogoutRedirectUriComponent', () => {
  let component: SegmentsPublicPostLogoutRedirectUriComponent;
  let fixture: ComponentFixture<SegmentsPublicPostLogoutRedirectUriComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPublicPostLogoutRedirectUriComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPublicPostLogoutRedirectUriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
