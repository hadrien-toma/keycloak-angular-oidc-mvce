import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateOtherVeryDeepRouteComponent } from './segments-private-other-very-deep-route.component';


describe('SegmentsPrivateOtherVeryDeepRouteComponent', () => {
  let component: SegmentsPrivateOtherVeryDeepRouteComponent;
  let fixture: ComponentFixture<SegmentsPrivateOtherVeryDeepRouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateOtherVeryDeepRouteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateOtherVeryDeepRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
