import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateOtherVeryDeepRouteComponent } from './components/segments-private-other-very-deep-route.component';

@NgModule({
	declarations: [SegmentsPrivateOtherVeryDeepRouteComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateOtherVeryDeepRouteComponent,
        children: [ ],
      },
    ]),
  ],
})
export class SegmentsPrivateOtherVeryDeepRouteModule {}
