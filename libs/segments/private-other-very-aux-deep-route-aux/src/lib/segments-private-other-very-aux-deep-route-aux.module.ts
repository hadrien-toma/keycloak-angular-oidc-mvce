import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateOtherVeryAuxDeepRouteAuxComponent } from './components/segments-private-other-very-aux-deep-route-aux.component';

@NgModule({
	declarations: [SegmentsPrivateOtherVeryAuxDeepRouteAuxComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateOtherVeryAuxDeepRouteAuxComponent,
        children: [ ],
      },
    ]),
  ],
})
export class SegmentsPrivateOtherVeryAuxDeepRouteAuxModule {}
