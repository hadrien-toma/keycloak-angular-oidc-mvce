import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateOtherVeryAuxDeepRouteAuxComponent } from './segments-private-other-very-aux-deep-route-aux.component';


describe('SegmentsPrivateOtherVeryAuxDeepRouteAuxComponent', () => {
  let component: SegmentsPrivateOtherVeryAuxDeepRouteAuxComponent;
  let fixture: ComponentFixture<SegmentsPrivateOtherVeryAuxDeepRouteAuxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateOtherVeryAuxDeepRouteAuxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateOtherVeryAuxDeepRouteAuxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
