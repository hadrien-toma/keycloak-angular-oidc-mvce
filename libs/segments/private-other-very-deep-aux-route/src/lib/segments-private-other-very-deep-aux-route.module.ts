import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateOtherVeryDeepAuxRouteComponent } from './components/segments-private-other-very-deep-aux-route.component';

@NgModule({
	declarations: [SegmentsPrivateOtherVeryDeepAuxRouteComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateOtherVeryDeepAuxRouteComponent,
        children: [ ],
      },
    ]),
  ],
})
export class SegmentsPrivateOtherVeryDeepAuxRouteModule {}
