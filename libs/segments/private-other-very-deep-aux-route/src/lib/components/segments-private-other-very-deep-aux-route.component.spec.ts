import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateOtherVeryDeepAuxRouteComponent } from './segments-private-other-very-deep-aux-route.component';


describe('SegmentsPrivateOtherVeryDeepAuxRouteComponent', () => {
  let component: SegmentsPrivateOtherVeryDeepAuxRouteComponent;
  let fixture: ComponentFixture<SegmentsPrivateOtherVeryDeepAuxRouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateOtherVeryDeepAuxRouteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateOtherVeryDeepAuxRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
