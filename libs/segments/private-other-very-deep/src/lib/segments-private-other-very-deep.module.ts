import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateOtherVeryDeepComponent } from './components/segments-private-other-very-deep.component';

@NgModule({
  declarations: [SegmentsPrivateOtherVeryDeepComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateOtherVeryDeepComponent,
        children: [
			{
			  path: '',
			  pathMatch: 'full',
			  redirectTo: 'route',
			},
          {
            path: 'route',
            loadChildren: () =>
              import('@workspace/segments-private-other-very-deep-route').then(
                (module) => module.SegmentsPrivateOtherVeryDeepRouteModule
              ),
          },
          {
            path: 'route-aux',
            loadChildren: () =>
              import(
                '@workspace/segments-private-other-very-deep-route-aux'
              ).then(
                (module) => module.SegmentsPrivateOtherVeryDeepRouteAuxModule
              ),
          },
		  {
			path: '**',
			redirectTo: 'route',
		  },
        ],
      },
    ]),
  ],
})
export class SegmentsPrivateOtherVeryDeepModule {}
