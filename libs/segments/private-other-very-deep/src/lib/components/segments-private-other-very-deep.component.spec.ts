import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateOtherVeryDeepComponent } from './segments-private-other-very-deep.component';


describe('SegmentsPrivateOtherVeryDeepComponent', () => {
  let component: SegmentsPrivateOtherVeryDeepComponent;
  let fixture: ComponentFixture<SegmentsPrivateOtherVeryDeepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateOtherVeryDeepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateOtherVeryDeepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
