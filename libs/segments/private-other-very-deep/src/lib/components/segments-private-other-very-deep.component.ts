import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'workspace-segments-private-other-very-deep',
  templateUrl: './segments-private-other-very-deep.component.html',
  styleUrls: ['./segments-private-other-very-deep.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SegmentsPrivateOtherVeryDeepComponent  {
}
