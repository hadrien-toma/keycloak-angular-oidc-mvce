import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateOtherVeryDeepRouteAuxComponent } from './components/segments-private-other-very-deep-route-aux.component';

@NgModule({
	declarations: [SegmentsPrivateOtherVeryDeepRouteAuxComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateOtherVeryDeepRouteAuxComponent,
        children: [ ],
      },
    ]),
  ],
})
export class SegmentsPrivateOtherVeryDeepRouteAuxModule {}
