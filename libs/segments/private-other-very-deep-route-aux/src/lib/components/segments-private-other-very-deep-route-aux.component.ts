import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'workspace-segments-private-other-very-deep-route-aux',
  templateUrl: './segments-private-other-very-deep-route-aux.component.html',
  styleUrls: ['./segments-private-other-very-deep-route-aux.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SegmentsPrivateOtherVeryDeepRouteAuxComponent  {
}
