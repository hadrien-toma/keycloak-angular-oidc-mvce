import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateOtherVeryDeepRouteAuxComponent } from './segments-private-other-very-deep-route-aux.component';


describe('SegmentsPrivateOtherVeryDeepRouteAuxComponent', () => {
  let component: SegmentsPrivateOtherVeryDeepRouteAuxComponent;
  let fixture: ComponentFixture<SegmentsPrivateOtherVeryDeepRouteAuxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateOtherVeryDeepRouteAuxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateOtherVeryDeepRouteAuxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
