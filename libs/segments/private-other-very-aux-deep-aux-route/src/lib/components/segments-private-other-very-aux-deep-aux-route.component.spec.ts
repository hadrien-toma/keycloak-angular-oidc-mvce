import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateOtherVeryAuxDeepAuxRouteComponent } from './segments-private-other-very-aux-deep-aux-route.component';


describe('SegmentsPrivateOtherVeryAuxDeepAuxRouteComponent', () => {
  let component: SegmentsPrivateOtherVeryAuxDeepAuxRouteComponent;
  let fixture: ComponentFixture<SegmentsPrivateOtherVeryAuxDeepAuxRouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateOtherVeryAuxDeepAuxRouteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateOtherVeryAuxDeepAuxRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
