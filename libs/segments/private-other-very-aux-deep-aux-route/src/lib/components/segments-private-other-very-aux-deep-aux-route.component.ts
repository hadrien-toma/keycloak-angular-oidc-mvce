import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'workspace-segments-private-other-very-aux-deep-aux-route',
  templateUrl: './segments-private-other-very-aux-deep-aux-route.component.html',
  styleUrls: ['./segments-private-other-very-aux-deep-aux-route.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SegmentsPrivateOtherVeryAuxDeepAuxRouteComponent  {
}
