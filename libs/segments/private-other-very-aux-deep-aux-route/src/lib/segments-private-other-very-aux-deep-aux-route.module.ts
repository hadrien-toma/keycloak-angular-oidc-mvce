import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateOtherVeryAuxDeepAuxRouteComponent } from './components/segments-private-other-very-aux-deep-aux-route.component';

@NgModule({
	declarations: [SegmentsPrivateOtherVeryAuxDeepAuxRouteComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateOtherVeryAuxDeepAuxRouteComponent,
        children: [
		],
      },
    ]),
  ],
})
export class SegmentsPrivateOtherVeryAuxDeepAuxRouteModule {}
