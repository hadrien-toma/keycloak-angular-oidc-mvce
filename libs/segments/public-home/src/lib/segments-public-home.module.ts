import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPublicHomeComponent } from './components/segments-public-home.component';

@NgModule({
	declarations: [SegmentsPublicHomeComponent],
  imports: [
    CommonModule,
	RouterModule.forChild([
		{
			path: '',
			component: SegmentsPublicHomeComponent,
			children: [	]
		}
	]),
  ],
})
export class SegmentsPublicHomeModule {}
