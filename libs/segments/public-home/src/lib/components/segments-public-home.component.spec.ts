import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPublicHomeComponent } from './segments-public-home.component';


describe('SegmentsPublicHomeComponent', () => {
  let component: SegmentsPublicHomeComponent;
  let fixture: ComponentFixture<SegmentsPublicHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPublicHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPublicHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
