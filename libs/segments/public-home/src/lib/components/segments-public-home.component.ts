import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'workspace-segments-public-home',
  templateUrl: './segments-public-home.component.html',
  styleUrls: ['./segments-public-home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SegmentsPublicHomeComponent  {
}
