import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'workspace-segments-private',
  templateUrl: './segments-private.component.html',
  styleUrls: ['./segments-private.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SegmentsPrivateComponent  {
}
