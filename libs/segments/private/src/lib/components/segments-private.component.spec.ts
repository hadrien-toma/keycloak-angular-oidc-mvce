import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateComponent } from './segments-private.component';


describe('SegmentsPrivateComponent', () => {
  let component: SegmentsPrivateComponent;
  let fixture: ComponentFixture<SegmentsPrivateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
