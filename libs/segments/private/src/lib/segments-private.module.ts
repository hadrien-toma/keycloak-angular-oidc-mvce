import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateComponent } from './components/segments-private.component';

@NgModule({
  declarations: [SegmentsPrivateComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateComponent,
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'admin',
          },
          {
            path: 'admin',
            loadChildren: () =>
              import('@workspace/segments-private-admin').then(
                (module) => module.SegmentsPrivateAdminModule
              ),
          },
          {
            path: 'other',
            loadChildren: () =>
              import('@workspace/segments-private-other').then(
                (module) => module.SegmentsPrivateOtherModule
              ),
          },
          {
            path: 'redirect-url',
            redirectTo: '/',
          },
          {
            path: '**',
            redirectTo: 'admin',
          },
        ],
      },
    ]),
  ],
})
export class SegmentsPrivateModule {}
