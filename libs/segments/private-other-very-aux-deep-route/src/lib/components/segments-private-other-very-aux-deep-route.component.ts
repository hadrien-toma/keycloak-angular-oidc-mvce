import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'workspace-segments-private-other-very-aux-deep-route',
  templateUrl: './segments-private-other-very-aux-deep-route.component.html',
  styleUrls: ['./segments-private-other-very-aux-deep-route.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SegmentsPrivateOtherVeryAuxDeepRouteComponent  {
}
