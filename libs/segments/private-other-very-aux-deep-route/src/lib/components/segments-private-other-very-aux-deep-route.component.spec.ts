import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateOtherVeryAuxDeepRouteComponent } from './segments-private-other-very-aux-deep-route.component';


describe('SegmentsPrivateOtherVeryAuxDeepRouteComponent', () => {
  let component: SegmentsPrivateOtherVeryAuxDeepRouteComponent;
  let fixture: ComponentFixture<SegmentsPrivateOtherVeryAuxDeepRouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateOtherVeryAuxDeepRouteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateOtherVeryAuxDeepRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
