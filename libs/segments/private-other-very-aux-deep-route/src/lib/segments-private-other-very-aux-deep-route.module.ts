import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateOtherVeryAuxDeepRouteComponent } from './components/segments-private-other-very-aux-deep-route.component';

@NgModule({
	declarations: [SegmentsPrivateOtherVeryAuxDeepRouteComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateOtherVeryAuxDeepRouteComponent,
        children: [
		],
      },
    ]),
  ],
})
export class SegmentsPrivateOtherVeryAuxDeepRouteModule {}
