import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'workspace-segments-private-other-very-aux',
  templateUrl: './segments-private-other-very-aux.component.html',
  styleUrls: ['./segments-private-other-very-aux.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SegmentsPrivateOtherVeryAuxComponent  {
}
