import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateOtherVeryAuxAuxComponent } from './segments-private-other-very-aux.component';


describe('SegmentsPrivateOtherVeryAuxComponent', () => {
  let component: SegmentsPrivateOtherVeryAuxComponent;
  let fixture: ComponentFixture<SegmentsPrivateOtherVeryAuxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateOtherVeryAuxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateOtherVeryAuxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
