import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateOtherVeryAuxComponent } from './components/segments-private-other-very-aux.component';

@NgModule({
  declarations: [SegmentsPrivateOtherVeryAuxComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateOtherVeryAuxComponent,
        children: [
			{
			  path: '',
			  pathMatch: 'full',
			  redirectTo: 'deep',
			},
          {
            path: 'deep',
            loadChildren: () =>
              import('@workspace/segments-private-other-very-aux-deep').then(
                (module) => module.SegmentsPrivateOtherVeryAuxDeepModule
              ),
          },
          {
            path: 'deep-aux',
            loadChildren: () =>
              import('@workspace/segments-private-other-very-aux-deep-aux').then(
                (module) => module.SegmentsPrivateOtherVeryAuxDeepAuxModule
              ),
          },
		  {
			path: '**',
			redirectTo: 'deep',
		  },
        ],
      },
    ]),
  ],
})
export class SegmentsPrivateOtherVeryAuxModule {}
