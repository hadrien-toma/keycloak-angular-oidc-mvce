import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateOtherVeryDeepAuxRouteAuxComponent } from './segments-private-other-very-deep-aux-route-aux.component';


describe('SegmentsPrivateOtherVeryDeepAuxRouteAuxComponent', () => {
  let component: SegmentsPrivateOtherVeryDeepAuxRouteAuxComponent;
  let fixture: ComponentFixture<SegmentsPrivateOtherVeryDeepAuxRouteAuxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateOtherVeryDeepAuxRouteAuxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateOtherVeryDeepAuxRouteAuxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
