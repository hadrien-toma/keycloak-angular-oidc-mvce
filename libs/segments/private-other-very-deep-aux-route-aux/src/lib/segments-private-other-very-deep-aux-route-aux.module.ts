import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateOtherVeryDeepAuxRouteAuxComponent } from './components/segments-private-other-very-deep-aux-route-aux.component';

@NgModule({
	declarations: [SegmentsPrivateOtherVeryDeepAuxRouteAuxComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateOtherVeryDeepAuxRouteAuxComponent,
        children: [ ],
      },
    ]),
  ],
})
export class SegmentsPrivateOtherVeryDeepAuxRouteAuxModule {}
