import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPublicComponent } from './components/segments-public.component';

@NgModule({
  declarations: [SegmentsPublicComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPublicComponent,
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'home',
          },
          {
            path: 'help',
            loadChildren: () =>
              import('@workspace/segments-public-help').then(
                (module) => module.SegmentsPublicHelpModule
              ),
          },
          {
            path: 'home',
            loadChildren: () =>
              import('@workspace/segments-public-home').then(
                (module) => module.SegmentsPublicHomeModule
              ),
          },
          {
            path: 'post-logout-redirect-uri',
            loadChildren: () =>
              import('@workspace/segments-public-post-logout-redirect-uri').then(
                (module) => module.SegmentsPublicPostLogoutRedirectUriModule
              ),
          },
          {
            path: '**',
            redirectTo: 'home',
          },
        ],
      },
    ]),
  ],
})
export class SegmentsPublicModule {}
