import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SegmentsPublicComponent } from './segments-public.component';

describe('SegmentsPublicComponent', () => {
  let component: SegmentsPublicComponent;
  let fixture: ComponentFixture<SegmentsPublicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPublicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPublicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
