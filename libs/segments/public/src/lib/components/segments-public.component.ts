import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'workspace-segments-public',
  templateUrl: './segments-public.component.html',
  styleUrls: ['./segments-public.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SegmentsPublicComponent  {
}
