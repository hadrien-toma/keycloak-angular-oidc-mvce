import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateOtherVeryAuxDeepAuxComponent } from './components/segments-private-other-very-aux-deep-aux.component';

@NgModule({
  declarations: [SegmentsPrivateOtherVeryAuxDeepAuxComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateOtherVeryAuxDeepAuxComponent,
        children: [
          {
            path: '',
			pathMatch: 'full',
            redirectTo: 'route',
          },
          {
            path: 'route',
            loadChildren: () =>
              import(
                '@workspace/segments-private-other-very-aux-deep-aux-route'
              ).then(
                (module) => module.SegmentsPrivateOtherVeryAuxDeepAuxRouteModule
              ),
          },
          {
            path: 'route-aux',
            loadChildren: () =>
              import(
                '@workspace/segments-private-other-very-aux-deep-aux-route-aux'
              ).then(
                (module) => module.SegmentsPrivateOtherVeryAuxDeepAuxRouteAuxModule
              ),
          },
          {
            path: '**',
            redirectTo: 'route',
          },
        ],
      },
      {
        path: 'segments-private-other-very-aux-deep-aux-route-aux',
        loadChildren: () =>
          import(
            '@workspace/segments-private-other-very-aux-deep-aux-route-aux'
          ).then(
            (module) => module.SegmentsPrivateOtherVeryAuxDeepAuxRouteAuxModule
          ),
      },
    ]),
  ],
})
export class SegmentsPrivateOtherVeryAuxDeepAuxModule {}
