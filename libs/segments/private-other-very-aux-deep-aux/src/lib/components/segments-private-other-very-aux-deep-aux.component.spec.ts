import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateOtherVeryAuxDeepAuxComponent } from './segments-private-other-very-aux-deep-aux.component';


describe('SegmentsPrivateOtherVeryAuxDeepAuxComponent', () => {
  let component: SegmentsPrivateOtherVeryAuxDeepAuxComponent;
  let fixture: ComponentFixture<SegmentsPrivateOtherVeryAuxDeepAuxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateOtherVeryAuxDeepAuxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateOtherVeryAuxDeepAuxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
