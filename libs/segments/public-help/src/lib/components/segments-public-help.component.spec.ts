import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPublicHelpComponent } from './segments-public-help.component';


describe('SegmentsPublicHelpComponent', () => {
  let component: SegmentsPublicHelpComponent;
  let fixture: ComponentFixture<SegmentsPublicHelpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPublicHelpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPublicHelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
