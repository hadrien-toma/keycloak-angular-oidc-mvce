import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'workspace-segments-public-help',
  templateUrl: './segments-public-help.component.html',
  styleUrls: ['./segments-public-help.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SegmentsPublicHelpComponent  {
}
