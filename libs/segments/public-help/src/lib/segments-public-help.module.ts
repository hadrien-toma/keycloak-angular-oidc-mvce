import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPublicHelpComponent } from './components/segments-public-help.component';

@NgModule({
	declarations: [SegmentsPublicHelpComponent],
  imports: [
    CommonModule,
	RouterModule.forChild([
		{
			path: '',
			component: SegmentsPublicHelpComponent,
			children: []
		}
	]),
  ],
})
export class SegmentsPublicHelpModule {}
