import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateOtherVeryComponent } from './components/segments-private-other-very.component';

@NgModule({
  declarations: [SegmentsPrivateOtherVeryComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateOtherVeryComponent,
        children: [
			{
			  path: '',
			  pathMatch: 'full',
			  redirectTo: 'deep',
			},
          {
            path: 'deep',
            loadChildren: () =>
              import('@workspace/segments-private-other-very-deep').then(
                (module) => module.SegmentsPrivateOtherVeryDeepModule
              ),
          },
          {
            path: 'deep-aux',
            loadChildren: () =>
              import('@workspace/segments-private-other-very-deep-aux').then(
                (module) => module.SegmentsPrivateOtherVeryDeepAuxModule
              ),
          },
		  {
			path: '**',
			redirectTo: 'deep',
		  },
        ],
      }
    ]),
  ],
})
export class SegmentsPrivateOtherVeryModule {}
