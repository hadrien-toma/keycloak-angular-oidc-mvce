import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'workspace-segments-private-other-very',
  templateUrl: './segments-private-other-very.component.html',
  styleUrls: ['./segments-private-other-very.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SegmentsPrivateOtherVeryComponent  {
}
