import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateOtherVeryComponent } from './segments-private-other-very.component';


describe('SegmentsPrivateOtherVeryComponent', () => {
  let component: SegmentsPrivateOtherVeryComponent;
  let fixture: ComponentFixture<SegmentsPrivateOtherVeryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateOtherVeryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateOtherVeryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
