import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'workspace-segments-private-other-very-deep-aux',
  templateUrl: './segments-private-other-very-deep-aux.component.html',
  styleUrls: ['./segments-private-other-very-deep-aux.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SegmentsPrivateOtherVeryDeepAuxComponent  {
}
