import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateOtherVeryDeepAuxComponent } from './segments-private-other-very-deep-aux.component';


describe('SegmentsPrivateOtherVeryDeepAuxComponent', () => {
  let component: SegmentsPrivateOtherVeryDeepAuxComponent;
  let fixture: ComponentFixture<SegmentsPrivateOtherVeryDeepAuxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateOtherVeryDeepAuxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateOtherVeryDeepAuxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
