import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateOtherVeryDeepAuxComponent } from './components/segments-private-other-very-deep-aux.component';

@NgModule({
  declarations: [SegmentsPrivateOtherVeryDeepAuxComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateOtherVeryDeepAuxComponent,
        children: [
          {
            path: '',
			pathMatch: 'full',
            redirectTo: 'route',
          },
          {
            path: 'route',
            loadChildren: () =>
              import(
                '@workspace/segments-private-other-very-deep-aux-route'
              ).then(
                (module) => module.SegmentsPrivateOtherVeryDeepAuxRouteModule
              ),
          },
          {
            path: 'route-aux',
            loadChildren: () =>
              import(
                '@workspace/segments-private-other-very-deep-aux-route-aux'
              ).then(
                (module) => module.SegmentsPrivateOtherVeryDeepAuxRouteAuxModule
              ),
          },
          {
            path: '**',
            redirectTo: 'route',
          },
        ],
      },
      {
        path: 'segments-private-other-very-deep-aux-route-aux',
        loadChildren: () =>
          import(
            '@workspace/segments-private-other-very-deep-aux-route-aux'
          ).then(
            (module) => module.SegmentsPrivateOtherVeryDeepAuxRouteAuxModule
          ),
      },
    ]),
  ],
})
export class SegmentsPrivateOtherVeryDeepAuxModule {}
