import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SegmentsPrivateOtherVeryAuxDeepAuxRouteAuxComponent } from './components/segments-private-other-very-aux-deep-aux-route-aux.component';

@NgModule({
	declarations: [SegmentsPrivateOtherVeryAuxDeepAuxRouteAuxComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SegmentsPrivateOtherVeryAuxDeepAuxRouteAuxComponent,
        children: [
		],
      },
    ]),
  ],
})
export class SegmentsPrivateOtherVeryAuxDeepAuxRouteAuxModule {}
