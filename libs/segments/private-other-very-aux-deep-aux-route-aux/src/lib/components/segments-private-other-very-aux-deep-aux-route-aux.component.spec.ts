import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SegmentsPrivateOtherVeryAuxDeepAuxRouteAuxComponent } from './segments-private-other-very-aux-deep-aux-route-aux.component';


describe('SegmentsPrivateOtherVeryAuxDeepAuxRouteAuxComponent', () => {
  let component: SegmentsPrivateOtherVeryAuxDeepAuxRouteAuxComponent;
  let fixture: ComponentFixture<SegmentsPrivateOtherVeryAuxDeepAuxRouteAuxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentsPrivateOtherVeryAuxDeepAuxRouteAuxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentsPrivateOtherVeryAuxDeepAuxRouteAuxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
