import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AuthModule, AutoLoginPartialRoutesGuard, LogLevel, StsConfigHttpLoader, StsConfigLoader } from 'angular-auth-oidc-client';
import { of } from 'rxjs';
import { AppComponent } from './app.component';

export const httpLoaderFactory = (httpClient: HttpClient) => {
	console.log("apps/dir-a/app-a/src/app/app.module.ts", window.location.origin.toString())
	//#region    keycloak
	const config$ = of({
		authority: "http://dev-station/yoptoruta/apps/company-aaa/name-aaa-keycloak/auth/realms/realm-aaa/.well-known/openid-configuration",
		clientId: "client-a",
		responseType: "code",
		scope: "openid profile email",
		startCheckSession: false,
		redirectUrl: 'http://localhost:8080/angular-oidc-mvce/private/redirect-url/',
		logLevel: LogLevel.Debug,
		maxIdTokenIatOffsetAllowedInSeconds: 10,
		historyCleanupOff: true,
		silentRenew: true,
		useRefreshToken: true,
		postLogoutRedirectUri: 'http://localhost:8080/angular-oidc-mvce/public/post-logout-redirect-uri/',
	}).toPromise();
	//#endregion keycloak

	return new StsConfigHttpLoader(config$);
  };

@NgModule({
  declarations: [AppComponent],
  imports: [
    AuthModule.forRoot({
		loader: {
		  provide: StsConfigLoader,
		  useFactory: httpLoaderFactory,
		  deps: [HttpClient],
		},
    }),
    BrowserModule,
    RouterModule.forRoot(
      [
        {
          path: '',
		  pathMatch: 'full',
          redirectTo: 'public',
        },
        {
          path: 'public',
          loadChildren: () =>
            import('@workspace/segments-public').then(
              (module) => module.SegmentsPublicModule
            ),
        },
        {
          path: 'private',
		  canActivate: [AutoLoginPartialRoutesGuard],
          loadChildren: () =>
            import('@workspace/segments-private').then(
              (module) => module.SegmentsPrivateModule
            ),
        },
        {
          path: '**',
          redirectTo: 'public',
        },
      ],
      {
        anchorScrolling: 'enabled',
        enableTracing: false,
        scrollPositionRestoration: 'enabled',
        initialNavigation: 'enabledBlocking',
      }
    ),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
