import { Component, OnInit } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';

@Component({
  selector: 'workspace-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
	constructor(public oidcSecurityService: OidcSecurityService) {
	}

	ngOnInit() {
	  // eslint-disable-next-line @typescript-eslint/no-unused-vars
	  this.oidcSecurityService.checkAuth().subscribe(({ isAuthenticated, userData, accessToken, idToken, configId, errorMessage }) => {
		  console.log('apps/dir-a/app-a/src/app/app.component.ts', 'ngOnInit', 'checkAuth', {isAuthenticated, userData, accessToken, idToken, configId, errorMessage})
	  });
	}

	login() {
	  this.oidcSecurityService.authorize();
	}

	logout() {
	  this.oidcSecurityService.logoff();
	}
  }